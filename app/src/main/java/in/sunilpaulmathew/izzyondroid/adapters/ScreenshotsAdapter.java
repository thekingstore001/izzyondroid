package in.sunilpaulmathew.izzyondroid.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.activities.ImageViewActivity;
import in.sunilpaulmathew.izzyondroid.utils.Common;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class ScreenshotsAdapter extends RecyclerView.Adapter<ScreenshotsAdapter.ViewHolder> {

    private static List<String> data;
    public ScreenshotsAdapter(List<String> data){
        ScreenshotsAdapter.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_screenshots, parent, false);
        return new ViewHolder(rowItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            Picasso.get().load(data.get(position)).into(holder.mImage);
        } catch (IndexOutOfBoundsException | NullPointerException ignored) {}
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AppCompatImageView mImage;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            this.mImage = view.findViewById(R.id.screenshot_image);
        }

        @Override
        public void onClick(View view) {
            if (Common.isDownloading()) return;
            Common.setScreenshotPosition(getAdapterPosition());
            Intent imageView = new Intent(view.getContext(), ImageViewActivity.class);
            view.getContext().startActivity(imageView);
        }
    }

}