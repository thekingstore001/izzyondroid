package in.sunilpaulmathew.izzyondroid.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.adapters.CategoryAdapter;
import in.sunilpaulmathew.izzyondroid.utils.RecyclerViewData;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class CategoryFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.layout_recyclerview, container, false);

        RecyclerView mRecyclerView = mRootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new GridLayoutManager(requireActivity(), RecyclerViewData.getSpanCount(6, 3, requireActivity())));
        mRecyclerView.setAdapter(new CategoryAdapter(RecyclerViewData.getCategories(requireActivity())));

        return mRootView;
    }

}